#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"

rm -rf build

mkdir build

cp -r "$(pwd)" /tmp/build

mv /tmp/build .

cd build

sh scripts/embeds.sh embeds embeds.html embeds_head.html

echo 'Executing setup...'
sh scripts/set.sh res/parser.conf docs/Chimmie-Firefly.md
sh scripts/set.sh res/parser.conf docs/about-me.md
sh scripts/set.sh res/parser.conf legacy/index.html
sh scripts/set.sh res/parser.conf embeds.html
sh scripts/set.sh res/parser.conf res/credits.html
sh scripts/set.sh res/parser.conf res/source.html
sh scripts/set.sh res/parser.conf search.js
sh scripts/set.sh res/parser.conf legacy/about-me/index.html
sh scripts/set.sh res/parser.conf README.md

rm -f .gitignore

echo 'Executing Indexing engine...'
export PARSER_FILE=res/parser.conf
export INDEX_PATH=docs
export OUTPUT_PATH="$(pwd)"
export CREDITS_FILE=res/credits.html
export SOURCE_FILE=res/source.html

bash scripts/index.sh
cp Chimmie-Firefly.html index.html
