#!/bin/bash

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
INDEX_PATH="${1:-$INDEX_PATH}"
OUTPUT_PATH="${2:-$OUTPUT_PATH}"
PARSER_FILE="${3:-$PARSER_FILE}"
CREDITS_FILE="${4:-$CREDITS_FILE}"
SOURCE_FILE="${5:-$SOURCE_FILE}"

if [ -z "$INDEX_PATH" ]; then
	echo "Index data path isn't specified."
	exit 1
fi

if [ -z "$OUTPUT_PATH" ]; then
	echo "Output path isn't specified."
	exit 2
fi

# Uses AWAKI_STYLES, AWAKI_ICON, AWAKI_AUTHOR, AWAKI_DESCRIPTION, AWAKI_DOMAIN
generate() {
	echo '<!DOCTYPE html>'
	echo '<html lang="en-US">'

	echo '<head>'
	echo '	<title>'"$__AWAKI_TITLE"'</title>'
	echo '	<meta name="icon" content="'"$AWAKI_ICON"'" />'
	echo '	<meta charset="UTF-8"/>'
	echo '	<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>'
	echo '	<meta http-equiv="X-UA-Compatible" content="IE=edge" />'
	echo '	<meta name="generator" content="Awaki Embeded" />'
	echo '	<meta property="og:title" content="'"$__AWAKI_TITLE"'" />'
	echo '	<meta name="author" content="'"$AWAKI_AUTHOR"'" />'
	echo '	<meta property="og:locale" content="en" />'
	echo '	<meta name="description" content="'"$AWAKI_DESCRIPTION"'" />'
	echo '	<meta property="og:description" content="'"$AWAKI_DESCRIPTION"'" />'
	echo '	<link rel="canonical" href="'"$AWAKI_DOMAIN"'/'"$__AWAKI_FILE"'" />'
	echo '	<meta property="og:url" content="'"$AWAKI_DOMAIN"'/'"$__AWAKI_FILE"'" />'
	echo '	<meta property="og:site_name" content="'"$__AWAKI_TITLE"'" />'
	echo '	<meta name="twitter:card" content="summary" />'
	echo '	<meta property="twitter:title" content="'"$__AWAKI_TITLE"'" />'
	echo '	<meta property="og:image" content="'"$AWAKI_ICON"'" />'
	echo '	<link rel="icon" href="'"$AWAKI_ICON"'" />'
	for X in $AWAKI_STYLES; do
		echo '	<link rel="stylesheet" href="'"$X"'"/>'
	done
	echo '</head>'

	echo '<body>'
	cat "$SOURCE_FILE"
	echo '<div class="freespace"></div>'
	echo '<div class="central"><div>'
	markdown "$1"
	echo '</div>'
    echo "$PAGE_INDEX"
    echo '</div>'
    cat "$CREDITS_FILE"
	echo '</body>'
	echo '</html>'

}

process_markdown() {
	FILE_PROCESS="$(basename "$1")"

	export __AWAKI_TITLE="$(echo "${FILE_PROCESS%.*}" | sed -e 's/-/ /g' -e 's/_/ /g')"
	first_letter="$(echo "${__AWAKI_TITLE:0:1}" | tr '[:lower:]' '[:upper:]')"
	last_string="${__AWAKI_TITLE:1}"
	export __AWAKI_TITLE="${first_letter}${last_string}"
	content="$(generate "$1")"
	echo "$content" > "${OUTPUT_PATH}/${FILE_PROCESS%.*}.html"
}

if ! [ -z "$PARSER_FILE" ]; then
	# Uses: &ENV_NAME&=<value>
	while IFS= read -r line; do
	    if [ -n "$line" ] && ! echo "$line" | grep -q "^#.*"; then
	        param_name="$(echo "$line" | cut -d "=" -f 1 | cut -f 2 -d '&')"
	        param_value="$(echo "$line" | cut -d "=" -f 2-)"

			if ! [ "${param_name:0:1}" == "%" ]; then
				eval "export $param_name='$param_value'"
			fi
	    fi
	done < "$PARSER_FILE"
fi

export PAGE_INDEX='<div class="index">'

for markdown_file in $(find "$INDEX_PATH" -maxdepth 1 -type f -name '*.md'); do
	if ! [ "$PAGE_INDEX" == '<div class="index">' ]; then
		export PAGE_INDEX="$PAGE_INDEX | "
	fi
	__filename="$(basename "${markdown_file%.*}")"
	__title="$(echo "${__filename%.*}" | sed -e 's/-/ /g' -e 's/_/ /g')"
	__first_letter="$(echo "${__title:0:1}" | tr '[:lower:]' '[:upper:]')"
	__last_string="${__title:1}"
	__title="${__first_letter}${__last_string}"
	export PAGE_INDEX="$PAGE_INDEX
	<a href="'"'"${__filename}.html"'"'">${__title}</a>"
done

export PAGE_INDEX="$PAGE_INDEX
</div>"

for markdown_file in $(find "$INDEX_PATH" -maxdepth 1 -type f -name '*.md'); do
	echo 'Processing '"$markdown_file"'...'
	process_markdown "$markdown_file"
done
