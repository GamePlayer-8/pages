<p align="center">
	<a href="https://chimmie.k.vu">
		<img src="res/README-icon.png" width="200">
		<h1 align="center">Chimmie main website</h1>
	</a>
	<hr>
</p>

The purpose of this repository is to provide a website for Chimmie Firefly, at [chimmie.k.vu](https://chimmie.k.vu).

### License

***Akini License 4.0***

