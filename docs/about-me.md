<div class="content">
<img src="https://libravatar.com/avatar/303e55d0083ff0576ae8c05d38752f11c20406361c06849b797e0780b3ecb835?s=400" alt=":avatar:" />
<div>
Heya again,
<br/><br/>
I'm Chimmie Firefly.<br/>
A little software developer and tester of commonly used open source software.<br/>
From time to time I tend to make some drawings and 3D models at <a href="https://xenia.chimmie.k.vu">xenia.chimmie.k.vu</a> and <a href="https://art.chimmie.k.vu">art.chimmie.k.vu</a>.<br/>
<br/>
Most of my life struggles or general talking I'm sharing at my <a href="https://catcatnya.com/@gameplayervent">Fediverse account</a> as well as on my <a href="https://mrrp.chimmie.k.vu/@gameplayer">own instance</a>.<br/>
<br/>
I don't have much of an experience in anything, really.<br/>
You can send me a message if you want to know more about me at my simple <a href="https://post.chimmie.k.vu">Post Mail system</a> or use my <a href="https://retrospring.net/@gameplayer">Retrospring</a>.<br/>
Asking me through Fediverse works as well. :3
</div>
</div>
