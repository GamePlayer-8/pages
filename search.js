const username = "GamePlayer-8"
const website = "https://chimmie.k.vu"

const apiUrl = "https://corsproxy.io/?https%3A%2F%2Fcodeberg.org%2Fapi%2Fv1%2Fusers%2F" + username + "%2Frepos%3Flimit%3D20%26page%3D"
// Initialize an empty array to store the results
const allData = [];

function getRandomElement(arr) {
  const randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
}

function openUrlGithub(element) {
  if (element) {
    var webpage = website + "/" + element
    fetch(webpage)
      .then(response => {
        if (response.ok) {
          window.open(webpage, '_blank');
        } else {
          window.open("https://codeberg.org/" + username + "/" + element, '_blank');
        }
      })
      .catch(error => {
        window.open("https://codeberg.org/" + username + "/" + element, '_blank');
    });
  }
}

function searchArray(arr, word) {
  let output = [];
  for (var i=0; i < arr.length; i++) {

    if (arr[i]["name"].includes(word)) {
      output.push(arr[i]["name"]);
    }
  }
  return output;
}

function createResultMenu(arr, menu) {
  menu.innerHTML = '';

  arr.forEach(element => {
    const link = document.createElement('a');
    link.textContent = element;
    link.href = website + "/" + element;
    menu.appendChild(link);
  });
}

// Define a function to fetch data from the API
async function fetchData(pageNumber) {
  try {
    const response = await fetch(apiUrl + pageNumber);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error fetching data:', error);
    return []; // Return an empty array if there's an error
  }
}

// Fetch data from each page until an empty array is returned
async function fetchAllData() {
  let pageNumber = 1;
  while (true) {
    const data = await fetchData(pageNumber);
    if (data.length === 0) {
      break; // Stop fetching when an empty array is returned
    }
    allData.push(...data); // Add data to the allData array
    pageNumber++;
  }
}

// Call the fetchAllData function
fetchAllData().then(() => {
    var json_data = allData;
    // You can now work with the data as a regular JavaScript object
    var console_text = document.getElementById('console-text');
    var search_menu = document.getElementById('search-content');
    var lucky_button = document.getElementById('lucky');
    var result_menu = document.getElementById('menu');

    console_text.textContent = 'Nyothing searched yet.';

    search_menu.style.display = 'inline-block';
    lucky_button.style.display = 'inline-block';

    lucky_button.addEventListener('click', function() {
      // This function will be executed when the button is clicked
      var content = getRandomElement(json_data);
      if (content.name) {
        // Open the link in a new tab or window
        openUrlGithub(content.name);
      } else {
        console.error('The .name property is missing or invalid in the JSON object.');
      }
    });

    // Add an event listener to listen for the "input" event
    search_menu.addEventListener('input', function() {
      // This function will be executed when the text in the input changes
      const enteredText = search_menu.value;
      if (enteredText !== "") {
        console_text.textContent = "Search results:";
        createResultMenu(searchArray(json_data, enteredText), result_menu);
      } else {
        console_text.textContent = "Nyothing searched.";
        result_menu.innerHTML = '';
      }
    });

  })
  .catch(error => {
    var console_text = document.getElementById('console-text');
    console_text.textContent = 'Fetching JSON has failed, check your logs!';
    console.error('Error fetching JSON:', error);
});

